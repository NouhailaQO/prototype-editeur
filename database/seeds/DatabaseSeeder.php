<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class, 10)->create();
        factory(App\Project::class, 500)->create();

        $projects = App\Project::all();

        App\User::all()->each(function ($user) use ($projects) {
            $user->projects()->attach(
                $projects->random(rand(1, 30))->pluck('id')->toArray()
            );
        });

        /*DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10) . '@gmail.com',
            'password' => Hash::make('test'),
        ]);*/

        //$this->call(ProjectSeeder::class);
    }
}
