/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import vuetify from './plugins/vuetify'
import store from "./store";
import Routes from './routes';
import Vuetify from "vuetify";
import VueResource from 'vue-resource';
import App from "./App.vue";
//import VueAxios from 'vue-axios';
import VueAuth from '@websanova/vue-auth'
import bearerAuth from '@websanova/vue-auth/drivers/auth/bearer'
import resourceAuth from '@websanova/vue-auth/drivers/http/vue-resource.1.x'
import routerAuth from '@websanova/vue-auth/drivers/router/vue-router.2.x'

Vue.use(VueResource);
Vue.http.options.root = "http://127.0.0.1:8000/api";
//Vue.use(VueAxios, axios);
Vue.use(Vuetify);

Vue.router = Routes;
Vue.use(VueAuth, {
    auth: bearerAuth,
    http: resourceAuth,
    router: routerAuth,
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    vuetify,
    router: Routes,
    store,
    render: h => h(App),
});
