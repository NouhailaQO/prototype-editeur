import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        projects: [],
        signIn: false,
        drawer: false,
    },
    mutations: {
        setProjects(state, data) {
            state.projects = data
        },
        updateDrawer(state) {
            state.drawer = !state.drawer
        },
        updateSignIn(state, newValue) {
            state.signIn = newValue;
        },
        addProject(state, project) {
            state.projects.push(project)
        },
        deleteProject(state, projectId) {
            state.projects = state.projects.filter(el => el.id !== projectId);
        },
        updateProject(state, project) {
            state.projects[state.projects.findIndex(el => el.id === project.id)] = project;
        },
    },
    actions: {}
})

export default store;
