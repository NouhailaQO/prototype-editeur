import Vue from 'vue';
import VueRouter from "vue-router";
import ExampleComponent from "./components/ExampleComponent";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import EditorComponent from "./components/EditorComponent";
import ProjectIndexComponent from "./components/projects/ProjectIndexComponent";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/projects',
            name: 'projects',
            component: ProjectIndexComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },
        {
            path: '/about',
            name: 'about',
            component: ExampleComponent,
        },
        {
            path: '/',
            name: 'home',
            component: EditorComponent,
        },
    ]
});

export default router;
