<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## About Prototyping Sketch Tool

ProtSketch is a web application with Laravel framework. Many tasks used in this web projects, such as:

- Prototyping ```(1st challenge)```
- Dashboard Projects
- User Projects

Requirements
--------------

Resources:

  * DATABASE CONFIG : ```DB_DATABASE``` at ```.env``` file.

Commands:

  * composer update --ignore-platform-reqs
  * php artisan doctrine:schema:update --force
  * php artisan migrate:fresh --force
  * php artisan db:seed
