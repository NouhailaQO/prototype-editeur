<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['codeXML'];

    //
    function project()
    {
        return $this->belongsTo('App\Project');
    }
}
