<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->projects;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $project = new Project();
        $project->name = $request->name;
        $project->save();

        $project->users()->attach(auth()->user());
        $project->save();
        auth()->user()->projects()->attach($project);
        auth()->user()->save();

        return response([
            'status' => 'success',
            'data' => $project
        ])->status(200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Project $project
     */
    public function update(Request $request, Project $project)
    {
        $project->name = $request->name;
        $project->save();

        return response([
            'status' => 'success',
            'data' => $project
        ])->status(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Project $project
     */
    public function destroy(Project $project)
    {
        auth()->user()->projects()->detach($project);
        Project::destroy($project);

        return response([
            'status' => 'success',
            'message' => 'deleted'
        ])->status(Response::HTTP_OK);
    }
}
