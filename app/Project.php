<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'users', 'pages'];

    function users()
    {
        return $this->belongsToMany('App\User');
    }

    function pages()
    {
        return $this->hasMany('App\Page');
    }
}
